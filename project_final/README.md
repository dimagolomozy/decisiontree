# **Data Analysis: Decision Tree**

By: **Dima Golomozy & Chemi Shumacher**  
Ben-Gurion University, Israel

Implementation of a decision tree algorithm for automatic analysis of large data sets, such as a large collection of text documents.  
The analysis will allow making predictions about the data.  
We will use ideas and tools from the field of machine learning. 

See **miniproject.pdf**, in "project_folder/docs" folder, for full project description and data file locations. (Note: Project description file was updated (typos in formulas fixed) on 18.3.15)  
More information can be found: [http://www.cs.bgu.ac.il/~tai152/Main](http://www.cs.bgu.ac.il/~tai152/Main)  

### Dependencies ###
1. numpy: [http://www.numpy.org/](http://www.numpy.org/)  
2. scipy: [http://www.scipy.org/](http://www.scipy.org/)  
3. nltk with English stopwords: [http://www.nltk.org/](http://www.nltk.org/)  
4. scikit-learn: [http://scikit-learn.org/stable/](http://scikit-learn.org/stable/)  
5. PyStemmer: [https://pypi.python.org/pypi/PyStemmer/1.0.1](https://pypi.python.org/pypi/PyStemmer/1.0.1)  

### Create environment
If all the dependencies are installed, just skip to step **Run**  

1. Install virtualenv:

        $ sudo apt-get install python-virtualenv
  
2. CD into project folder:

        $ cd project_folder
		
3. Run setup:

        $ ./setup.sh
        

### Run
1. To use the better Decision tree, just remove the underscore in "_DecisionTree2.py" (English stopwords corpus must be installed) 
2. In console run the command:  

        $ ./tree_learn train_directory test_file validation_percent tree_size output_file
        
        or:
          
        $ python tree_learn.py train_directory test_file validation_percent tree_size output_file
    
        args:
        train_directory - FULL path of the folder contains the *.train files  
        test_file - Test file name in the train_directory  
        validation_percent - Amount of percent to take to validation tests  
        tree_size - The max tree size = 2^(tree_size)  
        output_file - The output file name  
   
