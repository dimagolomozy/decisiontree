#!/bin/bash
virtualenv venv
source venv/bin/activate
pip install numpy
pip install pystemmer
pip install scikit-learn
pip install scipy
pip install nltk
python -m nltk.downloader all