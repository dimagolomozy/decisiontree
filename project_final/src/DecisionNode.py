#! /usr/bin/env python
"""
@subject: Data Analysis: Decision Tree
@authors: Chemi Shumacher and Dima Golomozy
"""

import numpy as np
from sklearn.metrics.cluster import entropy


class DecisionNode:
    """DecisionNode of a decision tree.

     Parameters:
    ------------
    *index* : node index. ordered by order of creation

    *l* : left DecisionNode child

    *r* : right DecisionNode child

    *word* : the word to be split by (for maximum information gain)

    *info gain* : information gain for *word*

    *label* : majority label of node

    *entropy* : entropy level of node
    
    *anti-words* : words to ignore. includes all words used for splitting in path from root to node.

    *train features* : list of train features
    """

    # static index for DecisionNodes
    index = 1

    @staticmethod
    def _get_index():
        """Returns the index, and increase it by 1 each return
        """
        old = DecisionNode.index
        DecisionNode.index += 1
        return old

    def __init__(self, train_features=None, anti_words=None):
        """
        Constructor. creates all private fields

        On creation calculates the majority label and the entropy of the train features.

         Parameters:
        ------------
        *train_features* : the train features

        *anti_word* : all the words to ignore
        """
        self.__index = DecisionNode._get_index()
        self.__l = None
        self.__r = None
        self.__word = None
        self.__info_gain = None
        self.__label = -1
        self.__entropy = 0
        self.__anti_words = anti_words
        self.__train_features = train_features

        if train_features is not None:
            self._calc_measures()

    def _calc_measures(self):
        """Calculate the label and the entropy of the node
        """
        # get all the labels from all the messages
        labels = [feature[0] for feature in self.__train_features]

        # calculate entropy and label
        if len(labels) != 0:
            self.__label = np.bincount(labels).argmax()
            self.__entropy = entropy(labels)

    def set_l(self, node):
        """Set the left child

         Parameters:
        ------------
        *node* : a DecisionNode
        """
        self.__l = node

    def set_r(self, node):
        """Set the right child

         Parameters:
        ------------
        *node* : a DecisionNode
        """
        self.__r = node

    def set_info_gain(self, info_gain):
        """Set the info gain

         Parameters:
        ------------
        *info_gain* : the info gain to set
        """
        self.__info_gain = info_gain

    def set_word(self, word):
        """Set the word to be split by it

         Parameters:
        ------------
        *word* : a number representing a word
        """
        self.__word = word

    def split_by_word(self, word):
        """Split the train features by the word

         Parameters:
        ------------
        *word* : a number representing a word

         Returns:
        ---------
        vector of 2 features [features_left, features_right]
        """
        features_l = []
        features_r = []
        for feature in self.__train_features:
            tmp_feature = feature[1]
            if word in tmp_feature.indices:     # indices = feature words
                features_l.append(feature)
            else:
                features_r.append(feature)

        return [features_l, features_r]

    def clone(self):
        """Recursive function the clones the DecisionNode

         Returns:
        ---------
        the new cloned DecisionNode
        """
        new_node = DecisionNode()
        new_node.__word = self.__word
        new_node.__info_gain = self.__info_gain
        new_node.__label = self.__label
        new_node.__entropy = self.__entropy
        new_node.__anti_words = self.__anti_words
        new_node.__train_features = self.__train_features
        if self.__l is not None:
            new_node.__l = self.__l.clone()
        if self.__r is not None:
            new_node.__r = self.__r.clone()
        return new_node

    def get_index(self):
        """Get the node index (creation order)
        """
        return self.__index

    def get_word(self):
        """Get the word to be split by
        """
        return self.__word

    def get_info_gain(self):
        """Get the info gain of the node
        """
        return self.__info_gain

    def split(self):
        """Split the train features by the self.__word

         Returns:
        ---------
        a vector of 2 features [features_left, features_right]
        """
        return self.split_by_word(self.__word)

    def get_entropy(self):
        """Get the entropy of the node
        """
        return self.__entropy

    def get_features(self):
        """Get the train features of the node
        """
        return self.__train_features

    def get_anti_words(self):
        """Get the anti_word of the node
        """
        return self.__anti_words

    def get_label(self):
        """Get the label of the node
        """
        return self.__label

    def get_l(self):
        """Get the left child of the node
        """
        return self.__l

    def get_r(self):
        """Get the right child of the node
        """
        return self.__r

    def is_leaf(self):
        """Check if the node is a leaf in the tree
        """
        return self.__l is None and self.__r is None