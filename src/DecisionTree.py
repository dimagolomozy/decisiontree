#! /usr/bin/env python
"""
@subject: Data Analysis: Decision Tree
@authors: Chemi Shumacher and Dima Golomozy
"""

import heapq
import math
import numpy as np
import random
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.cluster import entropy
from DecisionNode import DecisionNode
import sys
import time


class DecisionTree:
    """
    Creates a decision tree from train features.

    helps validate a message to its forum by self.__percent percent

     Properties:
    ------------
    *max tree size* : maximum tree size

    *tree size* : the actual tree size

    *percent* : percent of messages to be used as validation set

    *root* : the first DecisionNode
    """

    def __init__(self, training_messages_labels, training_messages, max_tree_size, validation_percent, is_random=False):
        """Constructor of a DecisionTree.

        Creates a vocabulary from the training messages

         Parameters:
        ------------
        *training_messages_labels* : all the training_messages labels in the right order as the training_messages

        *training_messages* : the training messages to be trained by

        *max_tree_size* : 2^max_tree_size will be the max tree size

        *validation_percent* : the percent of messages to take from training_messages for validation of the tree

        *is_random* : use read random function or take from "head" of the training_messages to validation (default = False)
        """
        self._create_vocabulary_messages(training_messages_labels, training_messages, validation_percent / float(100), is_random)
        self.__max_tree_size = math.pow(2, max_tree_size)
        self.__tree_size = None
        self.__percent = None
        self.__root = None

    def _create_vocabulary_messages(self, training_messages_labels, training_messages, validation_percent, is_random):
        """
        Calculate the vocabulary.

        Converts the training messages to vectors.

        Chooses validation_percent of messages to be validation train messages
        """
        # create vectorizer. for better runtime, limit number of words to top 20,000
        cv = CountVectorizer(analyzer="word",  # tokenize by word
                             max_features=10000)

        # fit_transform() does two functions: First, it fits the model
        # and learns the vocabulary; second, it transforms our training data into feature vectors.
        tmp_train_features = cv.fit_transform(training_messages)

        # attach reach train feature its label
        tmp_train_features = map(lambda l, f: [l, f], training_messages_labels, tmp_train_features)

        validation_features = []
        train_features = []
        if is_random:
            # split the tmp_train_features by validation_percent to take form validation_features
            for feature in tmp_train_features:
                if random.random() < validation_percent:
                    validation_features.append(feature)
                else:
                    train_features.append(feature)
        else:
            # calculate the amount of messages we need to take for validation_features
            labels = np.bincount(training_messages_labels)
            labels = map(lambda l: int(round(l * validation_percent)), labels)

            # split the tmp_train_features by validation_percent to take form validation_features
            for i in range(len(tmp_train_features)):
                feature = tmp_train_features[i]
                label = feature[0]
                if labels[label] != 0:
                    validation_features.append(feature)
                    labels[label] -= 1
                else:
                    train_features.append(feature)



        # set the vocab
        self.__vocab = cv.get_feature_names()
        # set the train and validation features
        self.__train_features = train_features
        self.__validation_features = validation_features

    def create_tree(self):
        """Start the creation of the tree based.
        """
        # create root decision node
        self.__root = DecisionNode(self.__train_features,  set())
        self.__tree_size = 1
        self.__percent = 0

        if self.__root.get_entropy() == 0:
            return

        # calculate the best word to split by of the node, and set it in the node
        info_gain = self._calc_best_word(self.__root)
        self.__root.set_info_gain(info_gain)

        # create and insert root to heap - minimum heap
        heap = []
        heapq.heappush(heap, (-info_gain, self.__root))

        # start main tree building function
        self._build_tree(heap)

    @staticmethod
    def _split_node(node):
        """Splits the node to left right node, and set them as the childes

         Returns:
        ---------
        the to nodes [node_left, node_right]
        """
        # split the node
        [features_l, features_r] = node.split()

        # create a new anti_words, and add the word that the node split the features by
        anti_words = node.get_anti_words() - set()
        anti_words.add(node.get_word())

        # create new left right nodes
        node_l = DecisionNode(features_l, anti_words)
        node_r = DecisionNode(features_r, anti_words)

        # set the to the current node
        node.set_l(node_l)
        node.set_r(node_r)

        return [node_l, node_r]

    def _calc_best_word(self, node):
        """Calculates the best word to split by and its information gain

         Returns:
        ---------
        the best information gain got by the chosen word
        """
        # if there are no features or entropy = 0, return 0
        if len(node.get_features()) == 0 or node.get_entropy() == 0:
            return 0

        best_word = None
        anti_words = node.get_anti_words()
        min_nodes_entropy = sys.maxsize
        for word in range(len(self.__vocab)):
            if word in anti_words:
                continue

            # split the node by the word, and return the splited features
            [features_l, features_r] = node.split_by_word(word)

            # calculate each features there entropy
            entropy_l = entropy([feature[0] for feature in features_l])
            entropy_r = entropy([feature[0] for feature in features_r])

            # calculate the total entropy of both features
            nodes_entropy = (len(features_l) * entropy_l) + (len(features_r) * entropy_r)
            nodes_entropy /= float(len(node.get_features()))

            # check if its better then the minimum, if so save it
            if nodes_entropy < min_nodes_entropy:
                min_nodes_entropy = nodes_entropy
                best_word = word

        # set the best word to split by in the node
        node.set_word(best_word)

        # return information gain
        return len(node.get_features()) * (node.get_entropy() - min_nodes_entropy)

    def validate_message(self, message):
        """Validate the message to the right "forum"

         Parameters:
        ------------
        *message* : the message to validate to a forum

         Returns:
        ---------
        the label of the chosen node
        """
        node = self.__root
        try:
            while not node.is_leaf():
                key = node.get_word()
                if self.__vocab[key] in message:
                    node = node.get_l()
                else:
                    node = node.get_r()
        finally:
            return node.get_label()

    def _validate_feature_indices(self, indices):
        """Finds the label of the feature based on the tree

        Choose left or right based on if the "label" of the node is
        in the feature.indices, and loops until we get to a leaf.

         Returns:
        ---------
        the label of the node
        """
        node = self.__root

        while not node.is_leaf():
            word = node.get_word()
            if word in indices:
                node = node.get_l()
            else:
                node = node.get_r()

        return node.get_label()

    def _validate_set(self):
        """
        Validates the tree with the validation_features.

        For each validation feature, runs it on the current tree and validates its label

         Returns:
        ---------
        percent of good validation
        """
        total = 0
        good = 0
        for feature in self.__validation_features:
            expected_label = feature[0]
            feature = feature[1]
            output_label = self._validate_feature_indices(feature.indices)

            if expected_label == output_label:
                good += 1
            total += 1

        val = good / float(total)
        
        total = 0
        good = 0
        for feature in self.__train_features:
            expected_label = feature[0]
            feature = feature[1]
            output_label = self._validate_feature_indices(feature.indices)

            if expected_label == output_label:
                good += 1
            total += 1

        train_val = good / float(total)
        return [val, train_val]
        
    def _build_tree(self, heap):
        """Main function that builds the tree.

        Pops the minimum DecisionNode from the heap, splits it, and pushes
        the left right sons to the heap for next iteration.

        Will loop until the size of the tree is maximum, or until the heap is empty.

        Each time the tree_size is 2^x, we print it, and calculate the validation of the tree with the
        validation_features. and the best tree is chosen.


         Parameters:
        ------------
        *heap* : the minimum heap to pop a node out
        """
        best_tree_copy = None
        best_tree_copy_size = 0
        step = 1
        #split_by_word_list_filename = os.path.join(training_folder, "split_word_list.csv")
        split_by_word_list = open("split_word_list.txt", 'w')
        
        while self.__tree_size < self.__max_tree_size and len(heap) != 0:
            # pop a node from the heap
            node = heapq.heappop(heap)[1]

            split_by_word_list.write("%s\n" % (self.__vocab[node.get_word()]))

            # if the entropy is 0, we don't need to split
            if node.get_entropy == 0:
                continue

            # split the node and set its sons, return a reference to the left right sons
            [node_l, node_r] = self._split_node(node)
            self.__tree_size += 2

            # calculate the best word of the nodes, and return there information gain by it
            info_gain_l = self._calc_best_word(node_l)
            info_gain_r = self._calc_best_word(node_r)

            # set the info gain to node, just for printing
            node_l.set_info_gain(info_gain_l)
            node_r.set_info_gain(info_gain_r)

            # if we got -1, that mean that the features of the node are empty. se skip the node
            if info_gain_l != 0:
                heapq.heappush(heap, (-info_gain_l, node_l))
            if info_gain_r != 0:
                heapq.heappush(heap, (-info_gain_r, node_r))

            # each tree size of 2^x up to max_tree_size we need to check with validation_features
            if self.__tree_size >= math.pow(2, step):
                values = self._validate_set()
                val = values[0]
                training_set_values = values[1]
                if val >= self.__percent:
                    self.__percent = val
                    best_tree_copy_size = self.__tree_size
                    best_tree_copy = self.__root.clone()
                step += 1
#               self.print_tree(sys.stdout)
                print("Done with tree size %d" % self.__tree_size)
                print("Validation set accuracy: %f" % (val*100) + "%")
                print("Training set accuracy: %f" % (training_set_values*100) + "%")
                print("-"*40)

        # save the best tree to be as root
        self.__tree_size = best_tree_copy_size
        self.__root = best_tree_copy
        print("Finished building tree:")
        print(">>> Training set size: %d " % len(self.__train_features))
        print(">>> Validation set size: %d = %f" % (len(self.__validation_features), 100 * len(self.__validation_features)/(len(self.__validation_features)+len(self.__train_features))) + "%")
        print(">>> Best validation set result: %f" % (self.__percent * 100) + "%")
        print(">>> Optimal tree size: %d with L=%d" % (self.__tree_size, int(math.log(self.__tree_size, 2))))

    def print_tree(self, fd):
        """Prints the tree to fd

         Parameters:
        ------------
        *fd* : file descriptor
        """
        print("\nPrints tree to fd: %s" % fd)
        print("Time: %s\n" % time.strftime("%H:%M:%S"))

        fd.write("Tree size %s with %s" % (self.__tree_size, self.__percent * 100) + "% success\n")
        if fd == sys.stdout:
            extension = "\t\t|"
        else:
            extension = "\t|"
        self._print_tree(self.__root, "|", extension, fd)

    def _print_tree(self, node, spaces, extension, fd):
        """Recursive function that prints the tree to the fd
        """
        if node is None:
            return

        new_spaces = spaces + extension
        fd.write("%s index: %s\n" % (spaces, node.get_index()))
        if node.get_word() is None:
            fd.write("%s word: None\n" % spaces)
        else:
            fd.write("%s word: %s (%s)\n" % (spaces, self.__vocab[node.get_word()], node.get_word()))
        fd.write("%s label: %s\n" % (spaces, node.get_label()))
        fd.write("%s size: %s\n" % (spaces, len(node.get_features())))
        fd.write("%s entropy: %s\n" % (spaces, node.get_entropy()))
        fd.write("%s info gain: %s\n" % (spaces, node.get_info_gain()))
        fd.write("%s left: \n" % spaces)
        self._print_tree(node.get_l(), new_spaces, extension, fd)
        fd.write("%s right: \n" % spaces)
        self._print_tree(node.get_r(), new_spaces, extension, fd)