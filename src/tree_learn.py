#! /usr/bin/env python
"""
@subject: Data Analysis: Decision Tree
@authors: Chemi Shumacher and Dima Golomozy
"""
import csv
import sys
import os
import time
import re
try:
    from DecisionTree2 import DecisionTree
except ImportError:
    from DecisionTree import DecisionTree


def check_args(training_folder, test_messages, validation_percent, max_tree_size):
    if not os.path.isdir(training_folder):
        raise SystemExit("Training Folder is not a directory")
    if sum((os.path.isfile(os.path.join(training_folder, f)) and f.endswith(".train"))
           for f in os.listdir(training_folder)) < 2:
        raise SystemExit("Training Folder had less then 2 .train files")
    if not os.path.isfile(os.path.join(training_folder, test_messages)):
        raise SystemExit("Test messages is not a file")
    if (not isinstance(validation_percent, int)) or (not 0 <= validation_percent <= 100):
        raise SystemExit("Validation percent is not in range 0..100")
    if not isinstance(max_tree_size, int):
        raise SystemExit("Max tree size is not a number")
    return


def main():
    argv = sys.argv
    if len(argv) != 6:
        raise SystemExit("Invalid usage: <training-folder-path> <test-file-path>"
                         " <percentage of validation> <max-tree-size> <output-file-name>\n")
    else:
        # Path for training example files. should contain at least 2
        training_folder = argv[1]
        print('Training folder: %s' % training_folder)
        # Path for test file examples
        test_messages = argv[2]
        print('Test examples file path: %s' % test_messages)
        # percentage of validation messages out of training set (range 0..100)
        validation_percent = int(argv[3])
        print('Validation set size: %s percent' % validation_percent)
        # Tree size range to test. Between 2^0 to 2^(maxTreeSize = L)
        max_tree_size = int(argv[4])
        print('Max tree size: 2^%s' % max_tree_size)
        # Output file name
        outfile = argv[5]
        print('Output file name: %s\n' % outfile)
        # Checking the args
        check_args(training_folder, test_messages, validation_percent, max_tree_size)

    # get the start time
    start_time = time.strftime("%H:%M:%S")

    # get the training messages and labels
    training = get_training_messages(training_folder)

    # create decision_tree based on the training
    print("Init decision tree...")
    decision_tree = DecisionTree(training[0], training[1], max_tree_size, validation_percent)
    print(">>> done")

    # save some memory
    del training

    # create the decision tree
    print("Creating tree...")
    decision_tree.create_tree()

    # print the decision tree
    print_path = os.path.join(training_folder, "decision_tree.txt")
    f = open(print_path, 'w')
    decision_tree.print_tree(f)
    f.close()

    # run decision tree on the test.example
    print("Running test example on tree...")
    run_test_example(decision_tree, training_folder, test_messages, outfile)
    print(">>> done\n")

    print("Run start time: %s" % start_time)
    print("Run finish time: %s" % time.strftime("%H:%M:%S"))
    [good, total] = compare_output(training_folder, outfile)
    print("Result: %s / %s = %f" % (good, total, (float(good)/float(total)) * 100) + "%")

    return


def compare_output(training_folder, outfile):
    total = 0
    good = 0
    confusion_matrix = [[0 for x in xrange(5)] for x in xrange(5)]
    with open(os.path.join(training_folder, outfile), 'r') as outfile:
        with open(os.path.join(training_folder, "test.labels"), 'r') as test_labels:
            for line_outfile, line_test_labels in zip(outfile, test_labels):
                if line_outfile == line_test_labels:
                    good += 1
                total += 1
                confusion_matrix[int(line_outfile)][int(line_test_labels)] += 1

    write_confusion_matrix(confusion_matrix, training_folder)     
    write_confusion_table(confusion_matrix, training_folder)     
    return [good, total]


def write_confusion_matrix(confusion_matrix, training_folder):
    confusion_path = os.path.join(training_folder, "confusion_matrix.csv")
    
    with open(confusion_path, 'w') as confusion_csv:
        fieldNames = ['guess\actual', '1', '2', '3', '4', 'correct']
        writer = csv.DictWriter(confusion_csv, fieldnames=fieldNames, lineterminator='\n')
        writer.writeheader()    
        for i in range(1, len(confusion_matrix)):
                row = ({'guess\actual':i, '1':confusion_matrix[i][1], '2':confusion_matrix[i][2], '3':confusion_matrix[i][3], '4':confusion_matrix[i][4], 'correct': confusion_matrix[i][i]})
                writer.writerow(row)


def write_confusion_table(confusion_matrix, training_folder):
    confusion_path = os.path.join(training_folder, "confusion_table.csv")
    
    with open(confusion_path, 'w') as confusion_csv:
        fieldNames = ['num', 'outputlabel', 'trueLabel']
        writer = csv.DictWriter(confusion_csv, fieldnames=fieldNames, lineterminator='\n')
        writer.writeheader()    
        for i in range(1,len(confusion_matrix)):
            for j in range(1,len(confusion_matrix)):
                row = ({'num':confusion_matrix[i][j], 'outputlabel':i, 'trueLabel':j})
                writer.writerow(row)
    
    
def run_test_example(decision_tree, training_folder, test_messages, outfile):
    with open(os.path.join(training_folder, outfile), 'w') as outfile:
        with open(os.path.join(training_folder, test_messages), 'r') as test_messages:
            for line in test_messages:
                label = decision_tree.validate_message(prepare_line(line))
                outfile.write("%s\n" % label)
    return


def get_training_messages(training_folder):
    # For each training file in folder, split by row and insert to labels
    # in correct indice corresponding to filename
    training_messages = []
    training_messages_label = []
    for train_file_name in os.listdir(training_folder):
        if train_file_name.endswith('.train'):
            full_file_name = os.path.join(training_folder, train_file_name)
            print('Reading training file: %s' % train_file_name)
            with open(full_file_name, 'r') as train_file:
                # Label is index of filename, eg. 1.train -> category 1
                index = int(train_file_name.split('.')[0])
                for line in train_file:
                    training_messages.append(prepare_line(line))
                    training_messages_label.append(index)

    print("")
    return [training_messages_label, training_messages]


def prepare_line(message):
    # Function to convert a message to a string of words
    # The input is a single string (message), and
    # the output is a single string (a preprocessed message)

    # CLEAN MESSAGES AND REMOVE NOISE
    email_regex = re.compile(("([a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`"
                              "{|}~-]+)*(@|\sat\s)(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?(\.|"
                              "\sdot\s))+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?)"))

    # 1. Remove all emails.
    no_email_message = re.sub(email_regex, " ", message)

    # 2. Remove non-letters
    letters_only = re.sub("[^a-zA-Z ]", " ", no_email_message)

    return letters_only.lower()


if __name__ == "__main__":
    main()
